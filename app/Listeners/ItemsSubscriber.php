<?php

namespace App\Listeners;

class ItemsSubscriber
{
    /**
     * Handle user login events.
     */
    public function onEntityCreated($event) {
        // project specific actions when new entity is created
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        // $events->listen(
        //     'Insolutions\Package\EventEntityCreated',
        //     'App\Listeners\ItemsSubscriber@onEntityCreated'
        // );

    }

}