<?php

namespace Insolutions\Items;

use Illuminate\Database\Eloquent\Model;

class Parameter extends Model
{    

	protected $table = 'enm_parameter';

	public $timestamps = false;

	public static function findByName($name) {
		return self::where(['name' => $name])->first();
	}
}
