<?php

Route::group(['prefix' => 'items'], function () {
	
	Route::get('entity', 'Insolutions\Items\Controller@getEntity');

	Route::put('items/{item_id}/tags/{tag_id}', 'Insolutions\Items\Controller@tagItem');
	Route::delete('items/{item_id}/tags/{tag_id}', 'Insolutions\Items\Controller@untagItem');
	Route::put('items/{item_id}/tags', 'Insolutions\Items\Controller@tagItem');

	Route::get('tags', 'Insolutions\Items\Controller@getTags');
	Route::post('tags', 'Insolutions\Items\Controller@tagSave');
	Route::put('tags/{tag_id}', 'Insolutions\Items\Controller@tagSave');

});