<?php

namespace Insolutions\Items;

use Illuminate\Database\Eloquent\Model;

class Service {

	private static function checkItemTrait(Model $model) {
		$traits = class_uses($model);

		if (!in_array(ItemTrait::class, $traits)) {
			Log::error("Model is NOT using trait.", [
				'model' => get_class($model),
				'trait' => ItemTrait::class
			]);
		}
	}

	public static function getTags(Model $model) {
		self::checkItemTrait($model);

		return $model->getItem()->tags;
	}

	public static function syncTags(Model $model, $tags) {
		self::checkItemTrait($model);

		$tagIds = [];
		foreach ($tags as $tag) {
			$tagIds[] = $tag->id;
		} 

		return $model->getItem()->tags()->sync($tagIds);
	}

	public static function addTag(Model $model, $tag) {
		self::checkItemTrait($model);

		return $model->getItem()->tags()->syncWithoutDetaching($tag->id);
	}

	public static function removeTag(Model $model, $tag) {
		self::checkItemTrait($model);

		return $model->getItem()->tags()->detach([$tag->id]);
	}
}