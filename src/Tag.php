<?php

namespace Insolutions\Items;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{    
	protected $table = 't_tag';

	protected $fillable = ['name'];

	protected $hidden = ['created_at', 'updated_at'];

	public static function findByName($name) {
		return self::where(['name' => $name])->first();
	}

	public static function getByName($name) {
		return self::firstOrCreate(['name' => $name]);
	}
}
