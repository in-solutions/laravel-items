<?php

namespace Insolutions\Items;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{	
	protected $table = 't_item';

	protected $fillable = ['entity', 'entity_id'];

	public $timestamps = false;

	public function getParameter($parameter) {
		return $this->getItemParam($parameter, false);
	}

	public function setParameter($parameter) {
		$this->getItemParam($parameter)->save();
	}

	public function hasParameter($parameter) {
		return ($this->getItemParam($parameter, false) !== null);
	}

	public function unsetParameter($parameter) {
		$itemParam = $this->getItemParam($parameter, false);
		if ($itemParam !== null) {
			$itemParam->delete();
		}
	}

	public function setParameterValue($parameter, $value) {
		$this->getItemParam($parameter)
			->setValue($value)
			->save();
	}

	public function getParameterValue($parameter) {
		return $this->getItemParam($parameter)->getValue();
	}

	public function setParameterRange($parameter, $value_min, $value_max) {
		$this->getItemParam($parameter)
			->setRange($value_min, $value_max)
			->save();
	}

	public function getParameterRange($parameter) {
		return $this->getItemParam($parameter)->getRange();
	}

	private function getItemParam($parameter, $createIfNotExists = true) {
		$param = Parameter::findByName($parameter);
		if (!$param) {
			throw new \InvalidArgumentException("Parameter '{$parameter}' not found!");
		}
		
		$identification = [
			'parameter_id' => $param->id,
			'item_id' => $this->id
		];

		$itemParam = $createIfNotExists
			? ItemParameter::firstOrCreate($identification)
			: ItemParameter::where($identification)->first();

		return $itemParam;
	}

	public function parameters() {
		return $this->hasMany('Insolutions\Items\ItemParameter');
	}

	public function tags() {
		return $this->belongsToMany('Insolutions\Items\Tag', 't_item_tag', 'item_id', 'tag_id');
	}
}
