<?php

namespace Insolutions\Items;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class ItemParameter extends Model
{    
	use SoftDeletes;

	protected $table = 't_item_parameter';

	public $fillable = [
		'item_id',
		'parameter_id',
		'number_from',
		'number_to',
		'text'
	];

	protected $casts = [
        'number_from' => 'double',
        'number_to' => 'double',
    ];

	public $dates = [
		'created_at',
		'updated_at',
		'deleted_at'
	];

	public function setValue($value) {
		if (is_numeric($value)) {
			$this->number_from = $this->number_to = $value;
			$this->text = null;
		} else {
			$this->text = $value;
			$this->number_from = $this->number_to = null;
		}

		return $this;
	}

	public function setRange($from, $to) {
		$this->number_from = $from;
		$this->number_to = $to;
		$this->text = null;

		return $this;
	}

	public function getValue() {
		return $this->number_from ? $this->number_from : $this->text; // should be equal to number_from
	}

	public function getRange() {
		return [$this->number_from, $this->number_to];
	}

	public function item() {
		return $this->belongsTo('Insolutions\Items\Item');
	}

	public function parameter() {
		return $this->belongsTo('Insolutions\Items\Parameter');
	}
	
}
