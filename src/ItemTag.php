<?php

namespace Insolutions\Items;

use Illuminate\Database\Eloquent\Model;

class ItemTag extends Model
{    
	protected $table = 't_item_tag';

	public $fillable = [
		'item_id',
		'tag_id'
	];

	public function item() {
		return $this->belongsTo('Insolutions\Items\Item');
	}

	public function tag() {
		return $this->belongsTo('Insolutions\Items\Tag');
	}
	
}
