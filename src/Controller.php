<?php

namespace Insolutions\Items;
 
use Illuminate\Http\Request;

use Auth;

class Controller extends \App\Http\Controllers\Controller
{
    
	public function getEntity(Request $r) {
		return response()->json(
			Item::with('tags')->paginate($r->perPage ?: 50)
		);	
	}

	public function tagSave(Request $r, $tag_id = null) {
		$tag = Tag::findByName($r->name);
		if ($tag && $tag->id != $tag_id) {	// if tag exists and it is not the one being updated
			return response("tag name already exists", 409);	// 409 because of: https://stackoverflow.com/questions/3825990/http-response-code-for-post-when-resource-already-exists
		}

		$tag = $tag_id ? Tag::findOrfail($tag_id) : new Tag;
		$tag->name = $r->name;
		$tag->parent_id = $r->parent_id ? Tag::findOrfail($r->parent_id)->id : null;
		$tag->save();

		return response()->json($tag);
	}

	public function tagItem(Request $r, $item_id, $tag_id = null) {
		$item = Item::findOrFail( $item_id );

		if ($tag_id) {
			$tag = Tag::findOrFail($tag_id);
			// $tag = Tag::getByName('found_or_created_tag_name');
			// $tag = Tag::findByName('found_tag_name');

			// only add tag
			$item->tags()->syncWithoutDetaching([ $tag->id ]);
		} else {
			$tagIds = [];

			foreach ($r->tags as $tag_id) {
				$tagIds[] = Tag::findOrFail($tag_id)->id;	// checking if existing
			}

			// sync tags (remove ones not present in array)
			$item->tags()->sync($tagIds);
		}
	}

	public function untagItem(Request $r, $item_id, $tag_id) {
		$tag = Tag::findOrFail($tag_id);
		// $tag = Tag::getByName('found_or_created_tag_name');
		// $tag = Tag::findByName('found_tag_name');

		Item::findOrFail( $item_id )->tags()->detach([ $tag->id ]);
	}

	public function getTags(Request $r) {
		$qb = Tag::query();

		if (isset($r->q)) {
			$qb->where('name', 'LIKE', "%{$r->q}%");
		}

		return response()->json(
			$qb->paginate($r->perPage ?: 50)
		);
	}

}