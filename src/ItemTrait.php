<?php

namespace Insolutions\Items;

trait ItemTrait
{	
	public function setParameter($param) {
		return $this->getItem()->setParameter($param);
	}

	public function hasParameter($param) {
		return $this->getItem()->hasParameter($param);
	}

	public function unsetParameter($param) {
		return $this->getItem()->unsetParameter($param);
	}

	public function setParameterValue($param, $value) {
		return $this->getItem()->setParametervalue($param, $value);
	}

	public function getParameterValue($param) {
		return $this->getItem()->getParameterValue($param);
	}

	public function setParameterRange($param, $value1, $value2) {
		return $this->getItem()->setParameterRange($param, $value1, $value2);
	}

	public function getParameterRange($param) {
		return $this->getItem()->getParameterRange($param);
	}

	public function getParameter($param) {
		return $this->getItem()->getParameter($param);
	}

	public function getItem() {
		return Item::firstOrCreate([
			'entity' => $this->getEntity(),
			'entity_id' => $this->getKey()
		]);
	}

	private function getEntity() {
		return get_class($this);
	}
}
