SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `enm_parameter` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('value','range','text','presence') COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='offset: 1000+ for project specific';

CREATE TABLE IF NOT EXISTS `t_item` (
`id` bigint(20) unsigned NOT NULL,
  `entity` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `entity_id` bigint(20) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `t_item_parameter` (
`id` bigint(20) unsigned NOT NULL,
  `item_id` bigint(20) unsigned NOT NULL,
  `parameter_id` int(11) unsigned NOT NULL,
  `number_from` decimal(20,4) DEFAULT NULL,
  `number_to` decimal(20,4) DEFAULT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `t_item`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `t_item_parameter`
 ADD PRIMARY KEY (`id`), ADD KEY `item_id` (`item_id`), ADD KEY `parameter_id` (`parameter_id`);


ALTER TABLE `t_item`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `t_item_parameter`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;

ALTER TABLE `t_item_parameter`
ADD CONSTRAINT `t_item_parameter_ibfk_2` FOREIGN KEY (`parameter_id`) REFERENCES `enm_parameter` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `t_item_parameter_ibfk_3` FOREIGN KEY (`item_id`) REFERENCES `t_item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
